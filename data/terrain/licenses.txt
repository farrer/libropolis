grass*.png
dirt_*.png
   Author: Cethiel
   License: CC0
   Sources: https://opengameart.org/content/tileable-grass-textures-set-1
            https://opengameart.org/content/tileable-dirt-textures

conc_01*.png
   Author: Spiney
   License: CC-BY-SA 3.0
   Source: https://opengameart.org/content/metalstone-textures




