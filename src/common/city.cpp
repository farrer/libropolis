/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "city.h"
#include <kobold/log.h>

namespace Libropolis
{
   /***********************************************************************
    *                         CityGridPosition                            *
    ***********************************************************************/
   CityGridPosition::CityGridPosition()
   {
   }
   
   /***********************************************************************
    *                         CityGridPosition                            *
    ***********************************************************************/
   CityGridPosition::~CityGridPosition()
   {
   }

   /***********************************************************************
    *                   CityGridPosition::addElement                      *
    ***********************************************************************/
   void CityGridPosition::addElement(CityElement* element)
   {
      elements.push_back(element);
   }
   
   /***********************************************************************
    *                   CityGridPosition::addElement                      *
    ***********************************************************************/
   void CityGridPosition::removeElement(CityElement* element)
   {
      elements.remove(element);
   }
   
   /***********************************************************************
    *                   CityGridPosition::hasElement                      *
    ***********************************************************************/
   bool CityGridPosition::hasElement(const CityElement::Type type)
   {
      for(std::list<CityElement*>::iterator it = elements.begin(); 
            it != elements.end(); ++it)
      {
         if((*it)->getType() == type)
         {
            return true;
         }
      }

      return false;
   }
   
   /***********************************************************************
    *                   CityGridPosition::hasElement                      *
    ***********************************************************************/
   bool CityGridPosition::hasElement(CityElement* element)
   {
      for(std::list<CityElement*>::iterator it = elements.begin(); 
            it != elements.end(); ++it)
      {
         if((*it) == element)
         {
            return true;
         }
      }

      return false;
   }

   /***********************************************************************
    *                              CityGrid                               *
    ***********************************************************************/
   CityGrid::CityGrid(int width, int height)
            :grid(width * height)
   {
      /* Define dimensions */
      this->width = width;
      this->height = height;
   }
   
   /***********************************************************************
    *                             ~CityGrid                               *
    ***********************************************************************/
   CityGrid::~CityGrid()
   {
   }

   /***********************************************************************
    *                           CityGrid::get                             *
    ***********************************************************************/
   CityGridPosition& CityGrid::get(int x, int y)
   {
      if((x >= 0) && (x < width) && (y >=0 ) && (y < height)) 
      {
         return this->grid[y * width + x];
      }
      Kobold::Log::add(Kobold::LOG_LEVEL_ERROR, 
            "Error: CityGrid::get called with invalid coordinates (%d, %d)",
            x, y);
      return this->grid[0];
   }
    
   /***********************************************************************
    *                        CityGrid::insert                             *
    ***********************************************************************/ 
   void CityGrid::insert(int x, int y, CityElement* element)
   {
      if((x >= 0) && (x < width) && (y >=0 ) && (y < height)) 
      {
         this->grid[y * width + x].addElement(element);
      }
      else
      {
         Kobold::Log::add(Kobold::LOG_LEVEL_ERROR, 
               "Error: CityGrid::set called with invalid coordinates (%d, %d)",
               x, y);
      }
   }

   /***********************************************************************
    *                               City                                  *
    ***********************************************************************/
   City::City(int width, int height)
        :surfaceGrid(width, height),
         subterraneanGrid(width, height)
   {
      this->terrain = NULL;
   }

   /***********************************************************************
    *                              ~City                                  *
    ***********************************************************************/
   City::~City()
   {
      if(this->terrain != NULL)
      {
         delete this->terrain;
      }
   }
   
   /***********************************************************************
    *                       City::createTerrain                           *
    ***********************************************************************/
   void City::createTerrain(Ogre::RTShader::ShaderGenerator* shaderGenerator)
   {
      this->terrain = new Terrain(shaderGenerator);
   }

   /***********************************************************************
    *                      City::getGridCoordinate                        *
    ***********************************************************************/
   void City::getGridCoordinate(const Ogre::Real x, const Ogre::Real z,
         IntVec2& gridCoord)
   {
      /* Note: City terrain is centered located at world 0,0, thus we need to
       * translate world coordinates by half size to access the grid. */
      int gridX = ((int) x / GRID_CELL_SIZE) + (surfaceGrid.getWidth() / 2); 
      int gridZ = ((int) z / GRID_CELL_SIZE) + (surfaceGrid.getHeight() / 2); 

      gridCoord.set(gridX, gridZ);
   }

   /***********************************************************************
    *                     City::getWorldCoordinate                        *
    ***********************************************************************/
   void City::getWorldCoordinate(int gridX, int gridZ, 
         Ogre::Real& x, Ogre::Real& z)
   {
      x = (gridX - (surfaceGrid.getWidth() / 2)) * GRID_CELL_SIZE;
      z = (gridZ - (surfaceGrid.getHeight() / 2)) * GRID_CELL_SIZE;
   }

}

