/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _libropolis_city_h
#define _libropolis_city_h

#include "../graphics/terrain.h"
#include "intvec2.h"

#define GRID_CELL_SIZE  10

namespace Libropolis
{
   /*! A city Element */
   class CityElement
   {
      public:
         enum Type
         {
            ROAD,
            TRAIN_ROAD,
            METRO_ROAD,
            PIPE,
            BUILDING,
            RESIDENTIAL_AREA,
            COMMERCIAL_AREA,
            INDUSTRIAL_AREA
         };

         /*! Constructor
          * \param type element's type */
         CityElement(const Type type) { this->type = type; };
         /*! Destructor */
         virtual ~CityElement() {};

         /*! \return CityElement::Type */
         const Type& getType() { return type; };

      private:
         Type type; /**< Type of the city's element */
   };

   /*! A single position on the grid */
   class CityGridPosition
   {
      public:
         /*! Constructor */
         CityGridPosition();
         /*! Destructor */
         ~CityGridPosition();

         /*! Add a CityElement to the grid position */
         void addElement(CityElement* element);
         /*! Remove CityElement from the grid position */
         void removeElement(CityElement* element);
         /*! \return if the grid position has an element of a type */
         bool hasElement(const CityElement::Type type);
         /*! \return if the grid position has an specific element */
         bool hasElement(CityElement* element);

      private:
         std::list<CityElement*> elements; /**< Grid position elements */
   };

   /*! The city grid of city elements */
   class CityGrid
   {
      public:
         /*! Constructor
          * \param width city's width
          * \param height city's height */
         CityGrid(int width, int height);
         /*! Destructor */
         ~CityGrid();
         
         /*! Get CityElement at position (x, y) 
          * \return CityElement's pointer. */
         CityGridPosition& get(int x, int y);

         /*! Insert a CityElement at position (x, y) */
         void insert(int x, int y, CityElement* element);

         /*! \return grid width */
         const int getWidth() const { return width; };
         /*! \return grid height */
         const int getHeight() const { return height; };

      private:
         /*! Grid, indexed by (y * width + x) */
         std::vector<CityGridPosition> grid;
         int width;
         int height;
   };

   /*! The city representation. It consists of two CityGrids, one for
    * its surface (with roads, zone areas, etc) and another for its
    * subterranean (with water pipes,  metro, etc). */
   class City
   {
      public:
         /*! New city with (width, height) size */
         City(int width, int height);
         /*! Destructor */
         ~City();

         /*! \return grid representing the city's surface */
         CityGrid& getSurfaceGrid() { return surfaceGrid; };
         
         /*! \return grid representing the city's subterranean */
         CityGrid& getSubterraneanGrid() { return subterraneanGrid; };

         /*! Create city terrain */
         void createTerrain(Ogre::RTShader::ShaderGenerator* shaderGenerator);

         /*! \return current city terrain */
         Terrain* getTerrain() { return terrain; };

         /*! Convert world position (x, y) to grid coordinate (gridX, gridZ). */
         void getGridCoordinate(const Ogre::Real x, const Ogre::Real z,
               IntVec2& gridCoord);

         /*! COnvert grid position (gridX, gridY) to world coordinate (x, y) */
         void getWorldCoordinate(int gridX, int gridZ, 
               Ogre::Real& x, Ogre::Real& z); 

      private:
         Terrain* terrain; /**< City terrain */
         CityGrid surfaceGrid;  /**< Grid above terrain */
         /*! Grid under terrain (ie: for water pipes and metro) */
         CityGrid subterraneanGrid; 
   };

}

#endif

