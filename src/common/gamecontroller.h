/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _libropolis_game_controller_h
#define _libropolis_game_controller_h

#include "city.h"
#include "../graphics/terrain.h"

namespace Libropolis
{
   /*! The game controller, keeping current running game related
    * variables for global access. */
   class GameController
   {
      public:
         /*! Init controller use */
         static void init();
         /*! Finish conroller use */
         static void finish();

         /*! Set currently used Ogre::SceneManager pointer. */
         static void setSceneManager(Ogre::SceneManager* ogreSceneManager);
         /*! \return currently used Ogre::SceneManager pointer. */
         static Ogre::SceneManager* getSceneManager();
         
         /*! \return current terrain pointer or NULL if no terrain defined. */
         static Terrain* getTerrain();

         /*! Get current city */
         static City* getCity();
         /*! Create a new city (deleting the current existent one, if any).
          * \param width city's grid width
          * \param height city's grid height */
         static void createNewCity(int width, int height);

      private:
         static City* city; /**< Current playing city */
         static Ogre::SceneManager* sceneManager;
   };

}

#endif

