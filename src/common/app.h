/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _libropolis_app_h
#define _libropolis_app_h

#include <goblin/baseapp.h>
#include <farso/controller.h>
#include <farso/ogre3d/ogrerenderer.h>
#include <farso/ogre3d/ogreloader.h>
#include "../graphics/camerachecker.h"
#include "../graphics/road.h"
#include "../graphics/terrain.h"

namespace Libropolis
{
   class App : public Goblin::BaseApp
   {
      public:
         App(const Goblin::CameraConfig& cameraConfig);
         ~App();

      protected:
         const Ogre::String getApplicationUnixName() const override;
         const Ogre::String getBaseDataDir() const override;
         bool doCycleInit(int callCounter, bool& shouldAbort) override;
         const int getDefaultWindowWidth() const override;
         const int getDefaultWindowHeight() const override;

         void getDataDirectories(Ogre::String** dataDirectories,
               Ogre::String** dataGroups, int& total) override;

         void doLowMemoryClean() override;
         void doSendToBackground() override;
         void doSendToForeground() override;

         bool useMaterialListerner() override { return false; };

         const bool shouldUseKoboldI18n() const override { return false; };

         const bool shouldDoCameraInput() override;

         Ogre::ShadowTechnique getShadowTechnique() override;
         const Orientation getDefaultOrientation() const override;
         const int getMinWidthToUseDoubleSizedGui() const override;

         void doBeforeRender() override;
         void doAfterRender() override;

         bool shouldQuit() override;
      private:
         CameraChecker cameraChecker;

         RoadController roadController;

         Ogre::Vector3 mouseTerrain; /**< Current mouse coordinate on terrain */

         Farso::OgreRenderer* farsoRenderer;
         Farso::OgreLoader farsoLoader;
   };
}

#endif

