/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _libropolis_int_vec2_h
#define _libropolis_int_vec2_h

namespace Libropolis
{
   /*! A bidimensional integer vector */
   class IntVec2
   {
      public:
         /*! Constructor */
         IntVec2() { x = 0; y = 0; };
         /*! Constructor with values */
         IntVec2(int x, int y) { this->x = x; this->y = y; };

         /*! \return first component */
         int getX() const { return x; };
         /*! \return second component */
         int getY() const { return y; };
         /*! \return second component.
          * \note just a way to be cleaner to read operations between
          * grid and terrain coordinates which are in (x,z) plane */
         int getZ() const { return y; };
         /*! Set the vector */
         void set(int x, int y) { this->x = x; this->y = y; };

      private:
         int x;
         int y;
   };
}

#endif

