/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gamecontroller.h"

namespace Libropolis
{

   /***********************************************************************
    *                                 init                                *
    ***********************************************************************/
   void GameController::init()
   {
   }

   /***********************************************************************
    *                                finish                               *
    ***********************************************************************/
   void GameController::finish()
   {
      if(city != NULL)
      {
         delete city;
      }
   }

   /***********************************************************************
    *                           setSceneManager                           *
    ***********************************************************************/
   void GameController::setSceneManager(Ogre::SceneManager* ogreSceneManager)
   {
      sceneManager = ogreSceneManager;
   }

   /***********************************************************************
    *                           getSceneManager                           *
    ***********************************************************************/
   Ogre::SceneManager* GameController::getSceneManager()
   {
      return sceneManager;
   }

   /***********************************************************************
    *                              getTerrain                             *
    ***********************************************************************/
   Terrain* GameController::getTerrain()
   {
      if(city != NULL)
      {
         return city->getTerrain();
      }
      return NULL;
   }

   /***********************************************************************
    *                               getCity                               *
    ***********************************************************************/
   City* GameController::getCity()
   {
      return city;
   }
   
   /***********************************************************************
    *                            createNewCity                            *
    ***********************************************************************/
   void GameController::createNewCity(int width, int height)
   {
      if(city != NULL)
      {
         delete city;
      }
      city = new City(width, height);
   }

   /***********************************************************************
    *                           static attributes                         *
    ***********************************************************************/
   City* GameController::city = NULL;
   Ogre::SceneManager* GameController::sceneManager = NULL;

}

