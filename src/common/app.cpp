/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "app.h"
#include "gamecontroller.h"
#include "../simulator/micropolis.h"

namespace Libropolis
{

   App::App(const Goblin::CameraConfig& cameraConfig)
       :Goblin::BaseApp(cameraConfig)
   {
      farsoRenderer = NULL;
      Goblin::Camera::setChecker(&cameraChecker);
      GameController::init();
   }
   
   App::~App() 
   {
      GameController::finish();
      Farso::Controller::finish();
      if(farsoRenderer != NULL)
      {
         delete farsoRenderer;
      }
   }

   const Ogre::String App::getApplicationUnixName() const
   {
      return "Libropolis";
   }

   const Ogre::String App::getBaseDataDir() const
   {
      return "../data/";
   }

   void App::getDataDirectories(Ogre::String** dataDirectories,
         Ogre::String** dataGroups, int& total)
   {
      static Ogre::String dirs[] = {"farso", "terrain", "materials", "roads"};
      (*dataDirectories) = &dirs[0];
      static Ogre::String groups[] = {"farso", "terrain", "materials", "roads"};
      (*dataGroups) = &groups[0];
      total = 4;
   }

   bool App::doCycleInit(int callCounter, bool& shouldAbort)
   {
      /* Initialize Farso */
      farsoRenderer = new Farso::OgreRenderer(getSceneManager(), 
            ogreRoot->getRenderSystem());
      Farso::Controller::init(&farsoLoader, farsoRenderer, 
            getDefaultWindowWidth(), getDefaultWindowHeight(), 32, "");
      Farso::Controller::setCursor("cursor/sel.png");
      Farso::Controller::loadSkin("skins/clean.skin");

      /* Initialize Libropolis */
      GameController::setSceneManager(getSceneManager());

      /* TODO: add city creation to its own action */
      //FIXME: remove micropolis' size restriction
      GameController::createNewCity(WORLD_W, WORLD_H);
      GameController::getCity()->createTerrain(shaderGenerator);

      return true;
   }

   const int App::getDefaultWindowWidth() const
   {
      return 800;
   }

   const int App::getDefaultWindowHeight() const
   {
      return 600;
   }

   void App::doLowMemoryClean()
   {
   }

   void App::doSendToBackground()
   {
   }

   void App::doSendToForeground()
   {
   }

   const bool App::shouldDoCameraInput()
   {
      return true;
   }

   Ogre::ShadowTechnique App::getShadowTechnique()
   {
      return Ogre::SHADOWTYPE_NONE;
   }

   const Goblin::BaseApp::Orientation App::getDefaultOrientation() const 
   {
      return LANDSCAPE; 
   }

   const int App::getMinWidthToUseDoubleSizedGui() const
   {
      return 50000;
   }

   void App::doBeforeRender()
   {
   }

   void App::doAfterRender()
   {
      if(Farso::Controller::verifyEvents(leftButtonPressed, rightButtonPressed,
            mouseX, mouseY))
      {
         //TODO: Treat events
      }
      else if(!Farso::Controller::wasMouseOverWidget()) 
      {

         /* Get cursor on world terrain */
         Ogre::Ray mouseRay;
         Goblin::Camera::getCameraToViewportRay(
               mouseX / Ogre::Real(ogreWindow->getWidth()),
               mouseY / Ogre::Real(ogreWindow->getHeight()), &mouseRay);

         Ogre::TerrainGroup* terrainGroup = 
            GameController::getTerrain()->getTerrainGroup();

         Ogre::TerrainGroup::RayResult rayResult = 
            terrainGroup->rayIntersects(mouseRay);
         if(rayResult.hit)
         {
            mouseTerrain = rayResult.position;
         }

         roadController.checkMouseAction(mouseTerrain, leftButtonPressed);
      }

      if(Kobold::Keyboard::isKeyPressed(Kobold::KOBOLD_KEY_H))
      {
         GameController::getTerrain()->getOgreTerrain()->_getRootSceneNode()->
            setVisible(false);
      }
      else if(Kobold::Keyboard::isKeyPressed(Kobold::KOBOLD_KEY_S))
      {
         GameController::getTerrain()->getOgreTerrain()->_getRootSceneNode()->
            setVisible(true);
      }
   }

   bool App::shouldQuit() 
   {
      return false;
   }

}

#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS &&\
    OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
/***********************************************************************
 *                                main                                 *
 ***********************************************************************/
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
      LPSTR lpCmdLine, int nCmdShow)
#else
int main (int argc, char *argv[])
#endif
{
   /* Create and run the game */
   Goblin::CameraConfig cameraConfig;

   cameraConfig.linearAttenuation = 6.0f;
   cameraConfig.angularAttenuation = 0.25f;
   cameraConfig.zoomMin = 6000.0f;
   cameraConfig.zoomMax = 800.0f;
   cameraConfig.linearVelocity = 15.0f;
   cameraConfig.angularVelocity = 2.0f;
   cameraConfig.zoomVelocity = 50.0f;
   cameraConfig.nearClipDistance = 40.0f;
   cameraConfig.farClipDistance = 50000.0f;
   cameraConfig.translateFactor = 2.0f;
   cameraConfig.maxDistToTranslate = 1.6f;
   cameraConfig.rotateFactor = 0.25f;
   cameraConfig.thetaMax = 89.0f;
   cameraConfig.thetaMin = 1.0f;

#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS &&\
    OGRE_PLATFORM != OGRE_PLATFORM_ANDROID
   cameraConfig.keyRotateLeft = Kobold::KOBOLD_KEY_COMMA;
   cameraConfig.keyRotateRight = Kobold::KOBOLD_KEY_PERIOD;
   cameraConfig.keyZoomIncrease = Kobold::KOBOLD_KEY_EQUALS;
   cameraConfig.keyZoomDecrease = Kobold::KOBOLD_KEY_MINUS;
   cameraConfig.keyMoveForward = Kobold::KOBOLD_KEY_UP;
   cameraConfig.keyMoveBackward = Kobold::KOBOLD_KEY_DOWN;
   cameraConfig.keyMoveLeft = Kobold::KOBOLD_KEY_LEFT;
   cameraConfig.keyMoveRight = Kobold::KOBOLD_KEY_RIGHT;
#endif
   Libropolis::App* app = new Libropolis::App(cameraConfig);
   app->run();
   delete app;
}
#endif

