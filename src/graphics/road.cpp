/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "road.h"
#include "../common/gamecontroller.h"
#include <kobold/log.h>

#define ROAD_WIDTH  10

namespace Libropolis
{
   /*************************************************************************
    *                              RoadMesh                                 *
    *************************************************************************/
   RoadMesh::RoadMesh()
            :CityElement(CityElement::ROAD)
   {
      manualObject = NULL;
      sceneNode = NULL;
      model3d = NULL;
   }
   
   /*************************************************************************
    *                             ~RoadMesh                                 *
    *************************************************************************/
   RoadMesh::~RoadMesh()
   {
      if(manualObject != NULL)
      {
         sceneNode->detachObject(manualObject);
         delete manualObject;
      }
      if(sceneNode != NULL)
      {
         GameController::getSceneManager()->destroySceneNode(sceneNode);
      }
      if(model3d != NULL)
      {
         delete model3d;
      }
   }

   /*************************************************************************
    *                          RoadMesh::create                             *
    *************************************************************************/
   void RoadMesh::create(const IntVec2& init, const IntVec2& end)
   {
      bool textureXOriented = true;
      if(manualObject != NULL)
      {
         Kobold::Log::add(Kobold::LOG_LEVEL_ERROR, 
               "Error: RoadMesh::create called on an already created mesh!");
         return;
      }
      Kobold::String name = "road" + Kobold::StringUtil::toString(++count);
      manualObject = new Ogre::ManualObject(name);
      
      /* Get initial and end points */
      IntVec2 initP((init.getX() <= end.getX()) ? init.getX() : end.getX(),
                    (init.getZ() <= end.getZ()) ? init.getZ() : end.getZ());
      IntVec2 endP((init.getX() > end.getX()) ? init.getX() : end.getX(),
                   (init.getZ() > end.getZ()) ? init.getZ() : end.getZ());

      /* Convert grid to world position */
      Ogre::Real iX=0.0f, iZ=0.0f, eX=0.0f, eZ=0.0f;
      GameController::getCity()->getWorldCoordinate(initP.getX(), initP.getZ(),
            iX, iZ);
      GameController::getCity()->getWorldCoordinate(endP.getX(), endP.getZ(),
            eX, eZ);

      //int xSize = 4 * ((eX - iX) + 1);
      //int zSize = 4 * ((eZ - iZ) + 1);

      int xSize = 20;
      int zSize = 20;

      /* Define road size */
      if(eX - iX > eZ - iZ)
      {
         eZ = iZ + ROAD_WIDTH;
         textureXOriented = false;
      }
      else 
      {
         eX = iX + ROAD_WIDTH;
         textureXOriented = true;
      }

      Ogre::Real incX = (eX - iX) / (Ogre::Real)(xSize);
      Ogre::Real incZ = (eZ - iZ) / (Ogre::Real)(zSize);

      printf("INC: %.3f %.3f\n", incX, incZ);

      Ogre::Terrain* terrain = GameController::getTerrain()->getOgreTerrain();
      manualObject->begin("RoadAsphalt", 
            Ogre::RenderOperation::OT_TRIANGLE_LIST, "roads");
      
      for(int i=0; i <= xSize; i++)
      {
         for(int j=0; j <= zSize; j++)
         {
            Ogre::Real cX = iX + (i * incX);
            Ogre::Real cZ = iZ + (j * incZ);
            Ogre::Real cY = terrain->getHeightAtWorldPosition(cX, 0.0f, cZ);

            manualObject->position(Ogre::Vector3(cX, cY + 2.0f, cZ)); 
            //manualObject->colour(1.0f, 1.0f, 1.0f, 1.0f);
            //FIXME: texture repeat!
            if(textureXOriented)
            {
               manualObject->textureCoord((float)i / (float)xSize,
                     (float)j / (float)zSize);
            }
            else 
            {
               manualObject->textureCoord((float)j / (float)xSize,
                     (float)i / (float)zSize);
            }
         }
      }

      for (int i=0; i < xSize; i++)
      {
         for (int j=0; j < zSize; j++)
         {
            manualObject->quad(i * (xSize+1) + j,
                  i * (xSize+1) + j + 1,
                  (i + 1) * (xSize+1) + j + 1,
                  (i + 1) * (xSize+1) + j);
         }
      }
      manualObject->end();

      sceneNode = GameController::getSceneManager()->getRootSceneNode()->
         createChildSceneNode();
      sceneNode->attachObject(manualObject);
      sceneNode->setVisible(true);
   }

   /*************************************************************************
    *                          RoadMesh::static                             *
    *************************************************************************/
   int RoadMesh::count = 0;


   /*************************************************************************
    *                           RoadController                              *
    *************************************************************************/
   RoadController::RoadController()
   {
      state = NONE;
   }
   
   /*************************************************************************
    *                           ~RoadController                             *
    *************************************************************************/
   RoadController::~RoadController()
   {
   }

   /*************************************************************************
    *                   RoadController::checkMouseAction                    *
    *************************************************************************/
   bool RoadController::checkMouseAction(const Ogre::Vector3& mouseTerrain, 
         bool leftButtonPressed)
   {
      if(leftButtonPressed) 
      {
         if(state == NONE)
         {
            state = ADD_ROAD;
            GameController::getCity()->getGridCoordinate(mouseTerrain.x,
                  mouseTerrain.z, initialPos);
         } 
         else if(state == ADD_ROAD)
         {
            /* TODO: update current draw */
         }
      } 
      else if(state == ADD_ROAD)
      {
         IntVec2 finalPos;
         GameController::getCity()->getGridCoordinate(mouseTerrain.x,
                  mouseTerrain.z, finalPos);
         
         RoadMesh* road = new RoadMesh();
         road->create(initialPos, finalPos);
         roads.insert(road);
         state = NONE;
      }
      return true;
   }
}

