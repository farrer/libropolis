/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _libropolis_road_h
#define _libropolis_road_h

#include <OGRE/OgreManualObject.h>
#include <kobold/list.h>
#include <goblin/model3d.h>
#include "../common/intvec2.h"
#include "../common/city.h"

namespace Libropolis
{

   /*! A road mesh. When directly on ground, it's defined as a 
    * Ogre::ManualObject applyed as decal on terrain. When not directly on the 
    * ground (bridges and tunnels), it's just a common mesh.
    * On junctions, the texture UV map is just translated to the 
    * correspondent junction part of the texture, keeping the whole 
    * mesh intact. 
    * For now, Libropolis is using one mesh for each user-created road (ie:
    * each click, drag and release will create a new RoadMesh).
    * @note: when terrain topology changes, it must affect the correspondent
    * RoadMeshes too. */
   class RoadMesh : public Kobold::ListElement, public CityElement
   {
      public:
         /*! Constructor */
         RoadMesh();
         /*! Destructor */
         virtual ~RoadMesh();

         /*! Create the road, from init(x,z) to end(x,z) */
         void create(const IntVec2& init, const IntVec2& end);

      private:
         Ogre::ManualObject* manualObject; /**< Road 3d decal representation */
         Ogre::SceneNode* sceneNode; /**< Scene node it is attached to */
         Goblin::Model3d* model3d; /**< 3d model */
         static int count; /**< Internal count for unique names */
   };

   /*! List of RoadMeshes */
   class Roads : public Kobold::List
   {
   };

   /*! The Roads controller, keeping trace of each RoadMesh. */
   class RoadController
   {
      public:
         /*! Constructor */
         RoadController();
         /*! Destructor */
         ~RoadController();

         bool checkMouseAction(const Ogre::Vector3& mouseTerrain, 
               bool leftButtonPressed);

      private:
         enum State
         {
            NONE,
            ADD_ROAD
         };
         Roads roads; /**< List of roads */
         State state; /**< Current state of the controller */
         IntVec2 initialPos;
   };

}

#endif

