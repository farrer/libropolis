/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _libropolis_camera_checker_h
#define _libropolis_camera_checker_h

#include <goblin/camera.h>

namespace Libropolis
{
   /*! Checker for camera position changes. Usualy, no allowing to go inner
    * the terrain or outside its bounds */
   class CameraChecker : public Goblin::CameraChecker
   {
      public:
         /*! Constructor */
         CameraChecker();
         /*! Destructor */
         ~CameraChecker();

         void verifyNewCameraPosition(Ogre::Vector3& newPos) override;
   };
}

#endif

