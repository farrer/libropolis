/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "terrain.h"
#include "../common/gamecontroller.h"
#include <OGRE/OgreMaterialManager.h>
#include <OGRE/OgreRoot.h>
#include <OGRE/OgreShadowCameraSetupPSSM.h>
#include <OGRE/OgreTextureManager.h>
#include <OGRE/RTShaderSystem/OgreShaderExIntegratedPSSM3.h>
#include <goblin/camera.h>

#define TERRAIN_FILE_PREFIX Ogre::String("testTerrain")
#define TERRAIN_FILE_SUFFIX Ogre::String("dat")
#define TERRAIN_WORLD_SIZE 12000.0f
#define TERRAIN_SIZE 513


namespace Libropolis
{

   Terrain::Terrain(Ogre::RTShader::ShaderGenerator* shaderGenerator)
           :mTerrainGlobals(0),
            mTerrainGroup(0),
            mMode(MODE_NORMAL),
            mLayerEdit(1),
            mBrushSizeTerrainSpace(0.02),
            mHeightUpdateCountDown(0),
            mTerrainPos(0,0,0),
            mTerrainsImported(false)

   {
      // Update terrain at max 20fps
      mHeightUpdateRate = 1.0 / 20.0;
      setupContent(shaderGenerator);
   }

   Terrain::~Terrain()
   {
      if(mTerrainGroup)
      {
         OGRE_DELETE mTerrainGroup;
         mTerrainGroup = 0;
      }

      if (mTerrainGlobals)
      {
         OGRE_DELETE mTerrainGlobals;
         mTerrainGlobals = 0;
      }
   }


   void Terrain::saveTerrains(bool onlyIfModified)
   {
      mTerrainGroup->saveAllTerrains(onlyIfModified);
   }

   void Terrain::doTerrainModify(Ogre::Terrain* terrain, 
         const Ogre::Vector3& centrepos, Ogre::Real timeElapsed)
   {
#if 0
      Ogre::Vector3 tsPos;
      terrain->getTerrainPosition(centrepos, &tsPos);
#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE_IOS
      if (mKeyPressed == '+' || mKeyPressed == '-' || mKeyPressed == SDLK_KP_PLUS ||
            mKeyPressed == SDLK_KP_MINUS)
      {
         switch(mMode)
         {
            case MODE_EDIT_HEIGHT:
               {
                  // we need point coords
                  Real terrainSize = (terrain->getSize() - 1);
                  long startx = (tsPos.x - mBrushSizeTerrainSpace) * terrainSize;
                  long starty = (tsPos.y - mBrushSizeTerrainSpace) * terrainSize;
                  long endx = (tsPos.x + mBrushSizeTerrainSpace) * terrainSize;
                  long endy= (tsPos.y + mBrushSizeTerrainSpace) * terrainSize;
                  startx = std::max(startx, 0L);
                  starty = std::max(starty, 0L);
                  endx = std::min(endx, (long)terrainSize);
                  endy = std::min(endy, (long)terrainSize);
                  for (long y = starty; y <= endy; ++y)
                  {
                     for (long x = startx; x <= endx; ++x)
                     {
                        Real tsXdist = (x / terrainSize) - tsPos.x;
                        Real tsYdist = (y / terrainSize)  - tsPos.y;

                        Real weight = std::min((Real)1.0,
                              Math::Sqrt(tsYdist * tsYdist + tsXdist * tsXdist) / Real(0.5 * mBrushSizeTerrainSpace));
                        weight = 1.0 - (weight * weight);

                        float addedHeight = weight * 250.0 * timeElapsed;
                        float newheight;
                        if (mKeyPressed == '+'  || mKeyPressed == SDLK_KP_PLUS)
                           newheight = terrain->getHeightAtPoint(x, y) + addedHeight;
                        else
                           newheight = terrain->getHeightAtPoint(x, y) - addedHeight;
                        terrain->setHeightAtPoint(x, y, newheight);

                     }
                  }
                  if (mHeightUpdateCountDown == 0)
                     mHeightUpdateCountDown = mHeightUpdateRate;
               }
               break;
            case MODE_EDIT_BLEND:
               {
                  TerrainLayerBlendMap* layer = terrain->getLayerBlendMap(mLayerEdit);
                  // we need image coords
                  Real imgSize = terrain->getLayerBlendMapSize();
                  long startx = (tsPos.x - mBrushSizeTerrainSpace) * imgSize;
                  long starty = (tsPos.y - mBrushSizeTerrainSpace) * imgSize;
                  long endx = (tsPos.x + mBrushSizeTerrainSpace) * imgSize;
                  long endy= (tsPos.y + mBrushSizeTerrainSpace) * imgSize;
                  startx = std::max(startx, 0L);
                  starty = std::max(starty, 0L);
                  endx = std::min(endx, (long)imgSize);
                  endy = std::min(endy, (long)imgSize);
                  for (long y = starty; y <= endy; ++y)
                  {
                     for (long x = startx; x <= endx; ++x)
                     {
                        Real tsXdist = (x / imgSize) - tsPos.x;
                        Real tsYdist = (y / imgSize)  - tsPos.y;

                        Real weight = std::min((Real)1.0,
                              Math::Sqrt(tsYdist * tsYdist + tsXdist * tsXdist) / Real(0.5 * mBrushSizeTerrainSpace));
                        weight = 1.0 - (weight * weight);

                        float paint = weight * timeElapsed;
                        size_t imgY = imgSize - y;
                        float val;
                        if (mKeyPressed == '+'  || mKeyPressed == SDLK_KP_PLUS)
                           val = layer->getBlendValue(x, imgY) + paint;
                        else
                           val = layer->getBlendValue(x, imgY) - paint;
                        val = Math::Clamp(val, 0.0f, 1.0f);
                        layer->setBlendValue(x, imgY, val);

                     }
                  }

                  layer->update();
               }
               break;
            case MODE_NORMAL:
            case MODE_COUNT:
               break;
         };
      }
#endif
#endif
   }

#if 0
   bool frameRenderingQueued(const FrameEvent& evt)
   {
      if (mMode != MODE_NORMAL)
      {
         // fire ray
         Ogre::Ray ray;
         //ray = mCamera->getCameraToViewportRay(0.5, 0.5);
         ray = mTrayMgr->getCursorRay(mCamera);

         Ogre::TerrainGroup::RayResult rayResult = mTerrainGroup->rayIntersects(ray);
         if (rayResult.hit)
         {
            mEditMarker->setVisible(true);
            mEditNode->setPosition(rayResult.position);

            // figure out which terrains this affects
            TerrainGroup::TerrainList terrainList;
            Real brushSizeWorldSpace = TERRAIN_WORLD_SIZE * mBrushSizeTerrainSpace;
            Sphere sphere(rayResult.position, brushSizeWorldSpace);
            mTerrainGroup->sphereIntersects(sphere, &terrainList);

            for (TerrainGroup::TerrainList::iterator ti = terrainList.begin();
                  ti != terrainList.end(); ++ti)
               doTerrainModify(*ti, rayResult.position, evt.timeSinceLastFrame);
         }
         else
         {
            mEditMarker->setVisible(false);
         }
      }

      if (!mFly)
      {
         // clamp to terrain
         Ogre::Vector3 camPos = mCameraNode->getPosition();
         Ogre::Ray ray;
         ray.setOrigin(Ogre::Vector3(camPos.x, mTerrainPos.y + 10000, camPos.z));
         ray.setDirection(Vector3::NEGATIVE_UNIT_Y);

         Ogre::TerrainGroup::RayResult rayResult = mTerrainGroup->rayIntersects(ray);
         Real distanceAboveTerrain = 50;
         Real fallSpeed = 300;
         Real newy = camPos.y;
         if (rayResult.hit)
         {
            if (camPos.y > rayResult.position.y + distanceAboveTerrain)
            {
               mFallVelocity += evt.timeSinceLastFrame * 20;
               mFallVelocity = std::min(mFallVelocity, fallSpeed);
               newy = camPos.y - mFallVelocity * evt.timeSinceLastFrame;

            }
            newy = std::max(rayResult.position.y + distanceAboveTerrain, newy);
            mCameraNode->setPosition(camPos.x, newy, camPos.z);

         }

      }

      if (mHeightUpdateCountDown > 0)
      {
         mHeightUpdateCountDown -= evt.timeSinceLastFrame;
         if (mHeightUpdateCountDown <= 0)
         {
            mTerrainGroup->update();
            mHeightUpdateCountDown = 0;

         }
      }

      //! [loading_label]
      if (mTerrainGroup->isDerivedDataUpdateInProgress())
      {
         mTrayMgr->moveWidgetToTray(mInfoLabel, TL_TOP, 0);
         mInfoLabel->show();
         if (mTerrainsImported)
         {
            mInfoLabel->setCaption("Building terrain, please wait...");
         }
         else
         {
            mInfoLabel->setCaption("Updating textures, patience...");
         }
      }
      else
      {
         mTrayMgr->removeWidgetFromTray(mInfoLabel);
         mInfoLabel->hide();
         if (mTerrainsImported)
         {
            // FIXME does not end up in the correct resource group
            // saveTerrains(true);
            mTerrainsImported = false;
         }
      }
      //! [loading_label]

      return SdkSample::frameRenderingQueued(evt);  // don't forget the parent updates!
   }
#endif

   void Terrain::defineTerrain(long x, long y, bool flat)
   {
      // if a file is available, use it
      // if not, generate file from import

      // Usually in a real project you'll know whether the compact terrain data is
      // available or not; I'm doing it this way to save distribution size

      if (flat)
      {
         mTerrainGroup->defineTerrain(x, y, 0.0f);
         return;

      }
      //! [define]
      Ogre::String filename = mTerrainGroup->generateFilename(x, y);
      if (Ogre::ResourceGroupManager::getSingleton().resourceExists(mTerrainGroup->getResourceGroup(), filename))
      {
         mTerrainGroup->defineTerrain(x, y);
      }
      else
      {
         Ogre::Image img;
         getTerrainImage(x % 2 != 0, y % 2 != 0, img);
         mTerrainGroup->defineTerrain(x, y, &img);
         mTerrainsImported = true;
      }
      //! [define]
   }

   void Terrain::getTerrainImage(bool flipX, bool flipY, Ogre::Image& img)
   {
      //! [heightmap]
      img.load("terrain.png", mTerrainGroup->getResourceGroup());
      if (flipX)
         img.flipAroundY();
      if (flipY)
         img.flipAroundX();
      //! [heightmap]
   }

   void Terrain::initBlendMaps(Ogre::Terrain* terrain)
   {
      //! [blendmap]
      Ogre::TerrainLayerBlendMap* blendMap0 = terrain->getLayerBlendMap(1);
      Ogre::TerrainLayerBlendMap* blendMap1 = terrain->getLayerBlendMap(2);
      float minHeight0 = 140;
      float fadeDist0 = 15;
      float minHeight1 = 420;
      float fadeDist1 = 15;
      float* pBlend0 = blendMap0->getBlendPointer();
      float* pBlend1 = blendMap1->getBlendPointer();
      for (Ogre::uint16 y = 0; y < terrain->getLayerBlendMapSize(); ++y)
      {
         for (Ogre::uint16 x = 0; x < terrain->getLayerBlendMapSize(); ++x)
         {
            Ogre::Real tx, ty;

            blendMap0->convertImageToTerrainSpace(x, y, &tx, &ty);
            float height = terrain->getHeightAtTerrainPosition(tx, ty);

            *pBlend0++ = Ogre::Math::saturate((height - minHeight0) / fadeDist0);
            *pBlend1++ = Ogre::Math::saturate((height - minHeight1) / fadeDist1);
         }
      }
      blendMap0->dirty();
      blendMap1->dirty();
      blendMap0->update();
      blendMap1->update();
      //! [blendmap]
      // set up a colour map
      /*
         if (!terrain->getGlobalColourMapEnabled())
         {
         terrain->setGlobalColourMapEnabled(true);
         Image colourMap;
         colourMap.load("testcolourmap.jpg", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
         terrain->getGlobalColourMap()->loadImage(colourMap);
         }
         */
   }

   void Terrain::configureTerrainDefaults(Ogre::Light* l)
   {
      Ogre::SceneManager* sceneManager = GameController::getSceneManager();
      //! [configure_lod]
      mTerrainGlobals->setMaxPixelError(8);
      mTerrainGlobals->setCompositeMapDistance(12000);
      //! [configure_lod]

      //mTerrainGlobals->setUseRayBoxDistanceCalculation(true);
      //mTerrainGlobals->getDefaultMaterialGenerator()->setDebugLevel(1);
      //mTerrainGlobals->setLightMapSize(256);

      // Disable the lightmap for OpenGL ES 2.0. The minimum number of samplers allowed is 8(as opposed to 16 on desktop).
      // Otherwise we will run over the limit by just one. The minimum was raised to 16 in GL ES 3.0.
      if (Ogre::Root::getSingletonPtr()->getRenderSystem()->getCapabilities()->getNumTextureUnits() < 9)
      {
         Ogre::TerrainMaterialGeneratorA::SM2Profile* matProfile =
            static_cast<Ogre::TerrainMaterialGeneratorA::SM2Profile*>(mTerrainGlobals->getDefaultMaterialGenerator()->getActiveProfile());
         matProfile->setLightmapEnabled(false);
      }

      //! [composite_lighting]
      // Important to set these so that the terrain knows what to use for baked (non-realtime) data
      mTerrainGlobals->setLightMapDirection(l->getDerivedDirection());
      mTerrainGlobals->setCompositeMapAmbient(sceneManager->getAmbientLight());
      mTerrainGlobals->setCompositeMapDiffuse(l->getDiffuseColour());
      //! [composite_lighting]
      //mTerrainGlobals->setCompositeMapAmbient(ColourValue::Red);

      // Configure default import settings for if we use imported image
      //! [import_settings]
      Ogre::Terrain::ImportData& defaultimp = mTerrainGroup->getDefaultImportSettings();
      defaultimp.terrainSize = TERRAIN_SIZE;
      defaultimp.worldSize = TERRAIN_WORLD_SIZE;
      defaultimp.inputScale = 600;
      defaultimp.minBatchSize = 33;
      defaultimp.maxBatchSize = 65;
      //! [import_settings]
      //! [textures]
      defaultimp.layerList.resize(3);
      
      defaultimp.layerList[0].worldSize = 100;
      Ogre::Image combined;
      combined.loadTwoImagesAsRGBA("grass_01.png", "grass_01_spec.png", 
            "General");
      Ogre::TextureManager::getSingleton().loadImage("grass_01_diffspec", 
            "General", combined);
      defaultimp.layerList[0].textureNames.push_back("grass_01_diffspec");
      combined.loadTwoImagesAsRGBA("grass_01_norm.png", "grass_01_disp.png", 
            "General");
      Ogre::TextureManager::getSingleton().loadImage("grass_01_normdisp", 
            "General", combined);
      defaultimp.layerList[0].textureNames.push_back("grass_01_normdisp");
      
      defaultimp.layerList[1].worldSize = 100;
      combined.loadTwoImagesAsRGBA("dirt_01.png", "dirt_01_spec.png", 
            "General");
      Ogre::TextureManager::getSingleton().loadImage("dirt_01_diffspec", 
            "General", combined);
      defaultimp.layerList[1].textureNames.push_back("dirt_01_diffspec");
      combined.loadTwoImagesAsRGBA("dirt_01_norm.png", "dirt_01_disp.png", 
            "General");
      Ogre::TextureManager::getSingleton().loadImage("dirt_01_normdisp", 
            "General", combined);
      defaultimp.layerList[1].textureNames.push_back("dirt_01_normdisp");

      defaultimp.layerList[2].worldSize = 100;
      combined.loadTwoImagesAsRGBA("conc_01.png", "conc_01_spec.png", 
            "General");
      Ogre::TextureManager::getSingleton().loadImage("conc_01_diffspec", 
            "General", combined);
      defaultimp.layerList[2].textureNames.push_back("conc_01_diffspec");
      combined.loadTwoImagesAsRGBA("conc_01_norm.png", "conc_01_disp.png", 
            "General");
      Ogre::TextureManager::getSingleton().loadImage("conc_01_normdisp", 
            "General", combined);
      defaultimp.layerList[2].textureNames.push_back("conc_01_normdisp");

      //! [textures]
   }

   void Terrain::changeShadows(Ogre::RTShader::ShaderGenerator* shaderGenerator)
   {
      configureShadows(shaderGenerator, mShadowMode != SHADOWS_NONE, 
            mShadowMode == SHADOWS_DEPTH);
   }

   void Terrain::configureShadows(
         Ogre::RTShader::ShaderGenerator* shaderGenerator, 
         bool enabled, bool depthShadows)
   {
      Ogre::SceneManager* sceneManager = GameController::getSceneManager();
      Ogre::Camera* camera = Goblin::Camera::getOgreCamera();
      Ogre::TerrainMaterialGeneratorA::SM2Profile* matProfile =
         static_cast<Ogre::TerrainMaterialGeneratorA::SM2Profile*>(mTerrainGlobals->getDefaultMaterialGenerator()->getActiveProfile());
      matProfile->setReceiveDynamicShadowsEnabled(enabled);
      //TODO: check if better or not enabled
      matProfile->setReceiveDynamicShadowsLowLod(false);

      Ogre::RTShader::RenderState* schemRenderState = shaderGenerator->getRenderState(Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME);

      for (auto srs : schemRenderState->getTemplateSubRenderStateList())
      {
         // This is the pssm3 sub render state -> remove it.
         if (dynamic_cast<Ogre::RTShader::IntegratedPSSM3*>(srs))
         {
            schemRenderState->removeTemplateSubRenderState(srs);
            break;
         }
      }

      if (enabled)
      {
         // General scene setup
         sceneManager->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED);
         sceneManager->setShadowFarDistance(3000);

         // 3 textures per directional light (PSSM)
         sceneManager->setShadowTextureCountPerLightType(Ogre::Light::LT_DIRECTIONAL, 3);

         if (!mPSSMSetup)
         {
            // shadow camera setup
            Ogre::PSSMShadowCameraSetup* pssmSetup = new Ogre::PSSMShadowCameraSetup();
            pssmSetup->setSplitPadding(camera->getNearClipDistance()*2);
            pssmSetup->calculateSplitPoints(3, camera->getNearClipDistance(), sceneManager->getShadowFarDistance());
            pssmSetup->setOptimalAdjustFactor(0, 2);
            pssmSetup->setOptimalAdjustFactor(1, 1);
            pssmSetup->setOptimalAdjustFactor(2, 0.5);

            mPSSMSetup.reset(pssmSetup);
         }
         sceneManager->setShadowCameraSetup(mPSSMSetup);

         if (depthShadows)
         {
            sceneManager->setShadowTextureCount(3);
            sceneManager->setShadowTextureConfig(0, 2048, 2048, Ogre::PF_FLOAT32_R);
            sceneManager->setShadowTextureConfig(1, 1024, 1024, Ogre::PF_FLOAT32_R);
            sceneManager->setShadowTextureConfig(2, 1024, 1024, Ogre::PF_FLOAT32_R);
            sceneManager->setShadowTextureSelfShadow(true);
            sceneManager->setShadowCasterRenderBackFaces(true);

            auto subRenderState = shaderGenerator->createSubRenderState<Ogre::RTShader::IntegratedPSSM3>();
            subRenderState->setSplitPoints(static_cast<Ogre::PSSMShadowCameraSetup*>(mPSSMSetup.get())->getSplitPoints());
            schemRenderState->addTemplateSubRenderState(subRenderState);

            sceneManager->setShadowTextureCasterMaterial(Ogre::MaterialManager::getSingleton().getByName("PSSM/shadow_caster"));
         }
         else
         {
            sceneManager->setShadowTextureCount(3);
            sceneManager->setShadowTextureConfig(0, 2048, 2048, Ogre::PF_X8B8G8R8);
            sceneManager->setShadowTextureConfig(1, 1024, 1024, Ogre::PF_X8B8G8R8);
            sceneManager->setShadowTextureConfig(2, 1024, 1024, Ogre::PF_X8B8G8R8);
            sceneManager->setShadowTextureSelfShadow(false);
            sceneManager->setShadowCasterRenderBackFaces(false);
            sceneManager->setShadowTextureCasterMaterial(Ogre::MaterialPtr());
         }

         matProfile->setReceiveDynamicShadowsDepth(depthShadows);
         matProfile->setReceiveDynamicShadowsPSSM(static_cast<Ogre::PSSMShadowCameraSetup*>(mPSSMSetup.get()));

         //addTextureShadowDebugOverlay(TL_RIGHT, 3);
      }
      else
      {
         sceneManager->setShadowTechnique(Ogre::SHADOWTYPE_NONE);
      }

      shaderGenerator->invalidateScheme(Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME);
   }

   void Terrain::setupContent(Ogre::RTShader::ShaderGenerator* shaderGenerator) 
   {
      Ogre::SceneManager* sceneManager = GameController::getSceneManager();
      //! [global_opts]
      mTerrainGlobals = new Ogre::TerrainGlobalOptions();
      //! [global_opts]

      Ogre::MaterialManager::getSingleton().setDefaultTextureFiltering(Ogre::TFO_ANISOTROPIC);
      Ogre::MaterialManager::getSingleton().setDefaultAnisotropy(7);

      //! [light]
      Ogre::Light* l = sceneManager->createLight("tstLight");
      l->setType(Ogre::Light::LT_DIRECTIONAL);
      l->setDiffuseColour(Ogre::ColourValue::White);
      l->setSpecularColour(Ogre::ColourValue(0.4, 0.4, 0.4));

      Ogre::SceneNode* ln = 
         sceneManager->getRootSceneNode()->createChildSceneNode();
      ln->setDirection(Ogre::Vector3(0.55, -0.3, 0.75).normalisedCopy());
      ln->attachObject(l);
      //! [light]
      sceneManager->setAmbientLight(Ogre::ColourValue(0.2, 0.2, 0.2));

      //! [terrain_create]
      mTerrainGroup = new Ogre::TerrainGroup(sceneManager, Ogre::Terrain::ALIGN_X_Z, TERRAIN_SIZE, TERRAIN_WORLD_SIZE);
      mTerrainGroup->setFilenameConvention(TERRAIN_FILE_PREFIX, TERRAIN_FILE_SUFFIX);
      mTerrainGroup->setOrigin(mTerrainPos);
      //! [terrain_create]

      configureTerrainDefaults(l);
      
      //! [define_loop]
      defineTerrain(0, 0, false);
      // sync load since we want everything in place when we start
      mTerrainGroup->loadAllTerrains(true);
      //! [define_loop]

      //! [init_blend]
      if (mTerrainsImported)
      {
         Ogre::TerrainGroup::TerrainIterator ti = mTerrainGroup->getTerrainIterator();
         while(ti.hasMoreElements())
         {
            Ogre::Terrain* t = ti.getNext()->instance;
            initBlendMaps(t);
         }
      }


      changeShadows(shaderGenerator);

      mTerrainGroup->freeTemporaryResources();
      //! [init_blend]
   }

   Ogre::Terrain* Terrain::getOgreTerrain()
   {
      Ogre::TerrainGroup::TerrainIterator ti = 
         mTerrainGroup->getTerrainIterator();
      if(ti.hasMoreElements())
      {
         return ti.getNext()->instance;
      }
      return NULL;
   }


}


