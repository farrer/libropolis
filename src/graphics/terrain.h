/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _libropolis_terrain_h
#define _libropolis_terrain_h

#include <OGRE/Paging/OgrePageManager.h>
#include <OGRE/Terrain/OgreTerrain.h>
#include <OGRE/Terrain/OgreTerrainGroup.h>
#include <OGRE/Terrain/OgreTerrainQuadTreeNode.h>
#include <OGRE/Terrain/OgreTerrainMaterialGeneratorA.h>
#include <OGRE/Terrain/OgreTerrainPaging.h>
#include <OGRE/RTShaderSystem/OgreRTShaderSystem.h>

namespace Libropolis
{

   class Terrain
   {
      public:
         Terrain(Ogre::RTShader::ShaderGenerator* shaderGenerator);
         ~Terrain();

         void doTerrainModify(Ogre::Terrain* terrain, 
               const Ogre::Vector3& centrepos, Ogre::Real timeElapsed);
    
         void saveTerrains(bool onlyIfModified);

         Ogre::Terrain* getOgreTerrain();

         Ogre::TerrainGroup* getTerrainGroup() { return mTerrainGroup; };
         const Ogre::Vector3& getPosition() const { return mTerrainPos; };

      protected:

         Ogre::TerrainGlobalOptions* mTerrainGlobals;
         Ogre::TerrainGroup* mTerrainGroup;
         enum Mode
         {
            MODE_NORMAL = 0,
            MODE_EDIT_HEIGHT = 1,
            MODE_EDIT_BLEND = 2,
            MODE_COUNT = 3
         };
         enum ShadowMode
         {
            SHADOWS_NONE = 0,
            SHADOWS_COLOUR = 1,
            SHADOWS_DEPTH = 2,
            SHADOWS_COUNT = 3
         };
         Mode mMode;
         ShadowMode mShadowMode;
         Ogre::uint8 mLayerEdit;
         Ogre::Real mBrushSizeTerrainSpace;
         Ogre::Real mHeightUpdateCountDown;
         Ogre::Real mHeightUpdateRate;
         Ogre::Vector3 mTerrainPos;
         bool mTerrainsImported;
         Ogre::ShadowCameraSetupPtr mPSSMSetup;

         void defineTerrain(long x, long y, bool flat = false);
         void getTerrainImage(bool flipX, bool flipY, Ogre::Image& img);
         void initBlendMaps(Ogre::Terrain* terrain);
         void configureTerrainDefaults(Ogre::Light* l);
         //void addTextureShadowDebugOverlay(Ogre::TrayLocation loc, size_t num);
         void changeShadows(Ogre::RTShader::ShaderGenerator* shaderGenerator);
         void configureShadows(Ogre::RTShader::ShaderGenerator* shaderGenerator,
               bool enabled, bool depthShadows);

         void setupContent(Ogre::RTShader::ShaderGenerator* shaderGenerator);
};

}

#endif

