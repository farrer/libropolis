/*
   Libropolis - A City Building Game
   Copyright (C) DNTeam <libropolis@dnteam.org>

   This file is part of Libropolis.

   Libropolis is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Libropolis is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Libropolis.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "camerachecker.h"
#include "../common/gamecontroller.h"

namespace Libropolis
{
   /***********************************************************************
    *                              Constructor                            *
    ***********************************************************************/
   CameraChecker::CameraChecker()
   {
   }

   /***********************************************************************
    *                               Destructor                            *
    ***********************************************************************/
   CameraChecker::~CameraChecker()
   {
   }

   /***********************************************************************
    *                       verifyNewCameraPosition                       *
    ***********************************************************************/
   void CameraChecker::verifyNewCameraPosition(Ogre::Vector3& newPos)
   {
      Terrain* terrain =  GameController::getTerrain();
      if(terrain != NULL)
      {
         Ogre::Vector3 camPos = newPos;
         Ogre::Ray ray;
         ray.setOrigin(Ogre::Vector3(camPos.x, 
                  terrain->getPosition().y + 10000, camPos.z));
         ray.setDirection(Ogre::Vector3::NEGATIVE_UNIT_Y);

         Ogre::TerrainGroup::RayResult rayResult = 
            terrain->getTerrainGroup()->rayIntersects(ray);
         Ogre::Real distanceAboveTerrain = 50;
         if(rayResult.hit)
         {
            if(camPos.y < rayResult.position.y + distanceAboveTerrain)
            {
               newPos.y = rayResult.position.y + distanceAboveTerrain;
            }
         }
      }
   }

}


